const fs = require("fs");

const Client = require('ssh2-sftp-client');
let sftp = new Client();
const remoteDir = '/PLNYI0C0/inbound/Relate/Reports/';
const couponReport = "Coupon_ECommerce_Report_Summary.xlsx";

const config = {
  host: 'ftpprodlnyisldc.oracleoutsourcing.com',
  port: '22',
  username: 'i_lnyi',
  privateKey : fs.readFileSync('./privatekey/OracleSFTPPublicKey_PA_ProdP.ppk')
}

;(function () {
  // code here
  downloadRelateReports();
})();

async function downloadRelateReports() {
  try {
    await sftp.connect(config);
    await sftp.fastGet(remoteDir + couponReport,"./recaps/"+couponReport);
  } catch (e) {
    console.log("err",e.message);
  } finally{
    await sftp.end();
  }
}
 
