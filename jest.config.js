module.exports = {
  preset: "jest-puppeteer",
  globals: {
    URL: "https://www.nyandcompany.com/",
  },
  testMatch: ["**/test/**/*.test.js"],
  verbose: true,
  setupFilesAfterEnv: ["./jest.setup.js"],
  reporters: [
    "default",
    [
      "./node_modules/jest-html-reporter",
      {
        pageTitle: "Test Report",
        outputPath: "reports/daily/index.html",
      },
    ],
  ],
};
