module.exports = {
  launch: {
    headless: true,
    args: ["--window-size=1920,1080"],
    slowMo: process.env.SLOWMO ? process.env.SLOWMO : 1,
    devtools: false,
    defaultViewport: null,
  },
  browserContext: "incognito",
  exitOnPageError: false,
};
