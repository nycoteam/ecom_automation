# Setup instructions

- Set the environment variable PUPPETEER_SKIP_CHROMIUM_DOWNLOAD

# Configuration options (jest-puppeteer.config.js)

- to avoid opening of Chrome window and run in background, set headless: true. By default, it's false

headless: true

- to open use , set Chrome path based on your Operating System.

chromeExecutablePath: "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"

# Commands to start tests

- Install by running _npm install_

- To execute tests, _npm run test_

# Setup private key to download recap reports from Remote (reports.download.js)

privateKey : fs.readFileSync('/path/OracleSFTPPublicKey_PA_ProdP.ppk')
