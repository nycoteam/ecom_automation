const fs = require("fs");
const path = require("path");
const moment = require("moment-timezone");
const excelToJson = require("convert-excel-to-json");

// site to domain mapping
const siteToDomain = {
  NYCO: "www.nyandcompany.com",
  FTF: "www.fashiontofigure.com",
  HXN: "www.happyxnature.com",
};

// get recap file name
const recapFileName = "Coupon_ECommerce_Report_Summary.xlsx";

// get recap file contents
const recapFileContents = excelToJson({
  sourceFile: "recaps/" + recapFileName,
  header: {
    rows: 6,
  },
});

// today's date
const today = moment().startOf('day').tz("America/New_York");

console.log("today", today);

// get valid coupons sitewise
const validCoupons = recapFileContents["Active_Summary"]
  .filter((i) => {
    const start = moment.tz(i.C, "MM/DD/YYYY", "America/New_York").subtract(1, "days");
    const end = moment
      .tz(i.D, "MM/DD/YYYY", "America/New_York")
      .subtract(1, "days");

    // console.log(
    //   i.A,
    //   i.B,
    //   i.C,
    //   i.D,
    //   i.O,
    //   start,
    //   end,
    //   i.O == "NO" && today.isBetween(start, end)
    // );

    return i.O == "NO" && today.isBetween(start, end, undefined, '[]');
  })
  .reduce((acc, cur) => {
    acc[cur.B] = acc[cur.B]
      ? [...acc[cur.B], cur.A]
      : cur.B == acc.B
      ? [acc.A, cur.A]
      : [cur.A];
    return acc;
  });

// get deactivated file name for today.
const deactivatedFileName = fs
  .readdirSync(path.join(__dirname, "../recaps"))
  .filter((i) => i.indexOf("Coupons to Deactivate") > -1);

// get contents from file
const deactivatedFileContents = excelToJson({
  sourceFile: "recaps/" + deactivatedFileName[0],
  header: {
    rows: 1,
  },
});

// fetch expired coupons from file
const expiredCoupons = deactivatedFileContents.Sheet1.map((i) =>
  i.C.toString()
);

// comon test data errors for all sites
const commonTest = {
  expectedErrors: [
    "SORRY THIS COUPON IS NOT VALID. IT MAY HAVE EXPIRED, BE UNCOMBINABLE WITH OUR CURRENT PROMOTION OR MIGHT ONLY BE VALID IN STORES.",
    "Sorry, we do not recognize the format of this coupon code. Please review the offer details and re-enter the code.",
  ],
};

const siteTestData = {
  [siteToDomain.NYCO]: {
    ...commonTest,
    style: "09752136",
    size: "XSMALL",
    validCoupons: validCoupons.NYCO,
    expiredCoupons: expiredCoupons,
    id:'NYCO'
  },
  [siteToDomain.FTF]: {
    ...commonTest,
    style: "05991424",
    size: "12",
    validCoupons: validCoupons.FTF,
    id:'FTF'
  },
};

console.log(siteTestData);

export default siteTestData;
