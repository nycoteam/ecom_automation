const fs = require("fs");
const moment = require("moment-timezone");

// Create test data.
const testData = require("../test-data/promotions.test.json");

// Transform test Data
const transformedTestData = testData.promotions.flatMap((promo) =>
  promo.promotedStyles.map((style) => {
    return { s: style, p: promo.promoTagLine };
  })
);

// Create directory for screenshots for today.
var directory;
(function createScreenshotsDir() {
  directory = `reports/screenshots/${moment
    .tz("America/New_York")
    .format("MM-DD")}`;
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory);
  }
  console.log("Created directory for today. - " + directory);
})();

// Start tests
describe("Test if promotaglines are appearing for products on Product Detail Page.", () => {
  test.each(transformedTestData)(
    "Promo tagline should be displayed for style %s %s",
    async (style) => {
      await testPromo(style.s, style.p);
    }
  );

  test.each(testData.nonPromotedStyles)(
    "Promo tagline should NOT be displayed for style %s as it is not on discount.",
    async (style) => {
      await testPromo(style);
    }
  );

  const testPromo = async (style, promotion) => {
    // go to pdp
    await page.goto(`https://www.nyandcompany.com/search/?Ntt=${style}`, {
      waitUntil: "load",
      timeout: 0,
    });

    await page.screenshot({
      path: `${directory}/${promotion && "no"}logicpromo_${style}_.png`,
    });

    // wait for navigation
    // await page.waitForNavigation({ waitUntil: "networkidle2" });

    // get tagline value
    const element = await page.$("span.promo_taglineone");

    const value = element
      ? await page.evaluate((element) => element.textContent, element)
      : undefined;

    // get text value of tagline displayed
    expect(value).toBe(promotion);
  };
});
