const fs = require("fs");
const moment = require("moment-timezone");

// Create test data.
const siteTests = require("../test-data/coupons.testdata").default;

// Create directory for screenshots for today.
var directory;
(function createScreenshotsDir() {
  directory = `reports/screenshots/${moment
    .tz("America/New_York")
    .format("MM-DD")}`;
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory);
  }
  console.log("Created directory for today. - " + directory);
})();

Object.entries(siteTests).forEach((i) => {
  const key = i[0],
    testData = i[1];

  // start test
  describe(
    "Test Coupons on Cart Page. applied if valid, error if not." + key,
    () => {
      if (testData.validCoupons)
        test.each(testData.validCoupons)(
          "Coupon is applied because it's a valid coupon %s",
          async (coupon) => {
            // add coupon to cart
            await addCoupon(coupon, "valid");

            // get error value
            const element = await page.$("#coupon-code-preview");
            const value = await page.evaluate(
              (element) => element.textContent,
              element
            );
            console.log("value", value, coupon);

            expect(value).toBe(`(${coupon})`);

            // remove coupon
            await removeCoupon(coupon);
          }
        );

      if (testData.expiredCoupons)
        test.each(testData.expiredCoupons)(
          "Error is displayed after applying expired coupon %s",
          async (coupon) => {
            // add coupon to cart
            await addCoupon(coupon, "error");

            // get error element
            const element = await page.$("#couponId");

            // get text value of error
            const value = await page.evaluate(
              (element) => (element ? element.textContent : ""),
              element
            );

            console.log(value, testData.expectedErrors.indexOf(value));

            // apply check on expected errors
            const check = testData.expectedErrors.indexOf(value) > -1;

            // assert check
            expect(check).toBeTruthy();
          }
        );

      //Call before the tests execute
      beforeAll(async () => {
        // go to pdp
        await page.goto(`https://${key}/search/?Ntt=${testData.style}`, {
          waitUntil: "load",
          timeout: 0,
        });

        await page.select("select#prod_prodsize", "2");
        await page.waitFor(500);

        await page.click("#atg_behaviour_addItemToCart");
        await page.waitFor(3000);

        await page.goto(`https://${key}/shopping-bag/`, {
          waitUntil: "load",
          timeout: 0,
        });
      });

      // helper function to add coupon to cart
      async function addCoupon(coupon, test) {
        // reset coupon and error values on page.
        await page.evaluate(() => {
          if(document.getElementById("couponText"))document.getElementById("couponText").value = "";
        });

        // enter coupon
        await page.type("input[id=couponText]", coupon, { delay: 200 });

        // click apply coupon button and wait for page to refresh
        await page.click("#couponApplyButton");

        // wait and take screenshot
        if (test === "valid") {
          await page.waitForNavigation({ waitUntil: "load" , timeout: 0});
        } else {
          await page.waitFor(10000);
        }
        await page.screenshot({
          path: `${directory}/${testData.id}_coupon_${coupon}_${test}.png`,
        });
      }

      // helper function to remove applied coupon from cart
      async function removeCoupon(coupon) {
        await Promise.all([
          page.$eval(`form[name=cartRemoveCouponForm_${coupon}]`, (form) => form.submit()),
          page.waitForNavigation()
         ])
        
      }
    }
  );
});

