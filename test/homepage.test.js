const timeout = 30000;

const testData = require("../test-data/homepage.test.json");

beforeAll(async () => {
  await page.goto(URL, { waitUntil: "domcontentloaded" });
});

describe("Test title on nyancompany home page", () => {
  test("Title of the page", async () => {
    const title = await page.title();
    expect(title).toBe(testData.title);
  });
});
