const fs = require("fs");
const sendmail = require("sendmail")();
const moment = require("moment-timezone");

const data = fs.readFileSync("reports/daily/index.html", "utf8");

sendmail(
  {
    from: "no-reply@retailwinds.com",
    to: "nycofeedsupport@ansr.com",
    subject:
      "NYCO: Daily Automation Test Report - " +
      moment().tz("America/New_York").format("DD MMM YYYY ha z"),
    html: data,
  },
  function (err, reply) {
    console.log(err && err.stack);
    console.log(reply);
  }
);
